//console.log("Hello")
//No.1
let studentOne = {
    name: "John",
    email: "john@gmail.com",
    grades: [89, 84, 78, 88],
    login (){
        console.log(`${this.email} has logged in`)
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    
    listGrades(){
       console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
    },
    computeAve(){
        const average = `${this.grades.reduce((a, b) => a + b, 0)}` / `${this.grades.length}`;
        return console.log(average)
    },
    willPass(){
        let pass = parseInt(this.computeAve());
        let boundary = parseInt(85);
        if(parseInt(pass) >= parseInt(boundary)){
            return pass;
        }
        else{
            return console.log(false)
        }
    },
    willPassWithHonors(){
        if(this.computeAve() >= parseInt(90)){
            return console.log(true)
        }
    }
}

let studentTwo = {
    name: "Joe",
    email: "joe@gmail.com",
    grades: [78, 82, 79, 85],
    login (){
        console.log(`${this.email} has logged in`)
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    
    listGrades(){
       console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
    },
    computeAve(){
        const average = `${this.grades.reduce((a, b) => a + b, 0)}` / `${this.grades.length}`;
        return console.log(average)
    },
    willPass(){
        let pass = parseInt(this.computeAve());
        let boundary = parseInt(85);
        if(parseInt(pass) >= parseInt(boundary)){
            return console.log(true)
        }
        else{
            return console.log(false)
        }
    }
}


let studentThree = {
    name: "Jane",
    email: "jane@gmail.com",
    grades: [87, 89, 91, 93],
    login (){
        console.log(`${this.email} has logged in`)
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    
    listGrades(){
       console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
    },
    computeAve(){
        const average = `${this.grades.reduce((a, b) => a + b, 0)}` / `${this.grades.length}`;
        return console.log(average)
    },
    willPass(){
        let pass = parseInt(this.computeAve());
        let boundary = parseInt(85);
        if(parseInt(pass) < parseInt(boundary)){
            return console.log(false)
        }
        else{
            return console.log(true)
        }
    }
}

let studentFour = {
    name: "Jessie",
    email: "jessie@gmail.com",
    grades: [91, 89, 92, 93],
    login (){
        console.log(`${this.email} has logged in`)
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    
    listGrades(){
       console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
    },
    computeAve(){
        const average = `${this.grades.reduce((a, b) => a + b, 0)}` / `${this.grades.length}`;
        return console.log(average)
    },
    willPass(){
        let pass = parseInt(this.computeAve());
        let boundary = parseInt(85);
        if(parseInt(pass) < parseInt(boundary)){
            return console.log(false)
        }
        else{
            return console.log(true)
        }
    },
    willPassWithHonors(){
        let me = this.computeAve()
        return console.log(me)
        // if(this.computeAve() >= parseInt(90)){
        //     return console.log(true)
        // }
    }
}



//Translate the other students from our boilerplate code into their own respective objects.

//Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

//Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

//Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

//Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

//Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

//Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

//Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

//Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.